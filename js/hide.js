function hideItem() {

	// if there is crossed out(completed) items
	if ($("input:checkbox:checked").length) {

		// hide all crossed out(completed) items
		$("input:checkbox:checked").parent('li').hide();
		$('#hide').hide();

		var show = '<button id="show"></button>';

		$('#add').after(show);

		localStorage.setItem('todo', $('#list').html()); // save session
	}
}
