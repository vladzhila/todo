function updateItem() {

	$('#list').find('li').show();

	// add text of item in span with id
	$('#editItem').text($('#newElem').val().trim());

	$('#update').remove(); // remove button 'update'
	$('#add').show(); // show button 'add'

	localStorage.setItem('todo', $('#list').html()); // save session

	if ($('#show').hasClass('is')) {
		hideItem();

		$('#show').show();
		$('button[class="fa fa-eye-slash is"]').remove();
	} else {
		$('#hide').show();
	}

	$('#newElem').val(''); // reset main input
	$('#editItem').removeAttr('id');
}