function addItem() {

	var checkbox = '<input type="checkbox">';

	// add item to span
	var span = '<span>' + $('#newElem').val().trim() + '</span>';
	var edit = '<button id="edit"></button>';
	var remove = '<button id="delete"></button>';

	// create item(li) with content 
	var li = '<li>' + checkbox + span + edit + remove + '</li>';

	$('#list').append(li);

	// reset main input
	$('#newElem').val('').focus();

	localStorage.setItem('todo', $('#list').html()); // save session
}