$(function(){

	$('.wrap').append('<ul id="list"></ul>');

	$('#list').html(localStorage.getItem('todo')); // loading session

	// when page loaded show all items
	$('#list').find('li').each(function(inx, val) {
		val.removeAttribute('style');
	});

		// ADD ITEM (PUSH BUTTON)

	$('#add').click(function() {

		// if something is written
		if ($('#newElem').val().trim().length) {
			addItem();
		} else {
			$('#newElem').focus();
		}
	});

		// ADD ITEM (PUSH ENTER)

		$('#newElem').keypress(function() {

			// if push 'ender' and something is written
			if (event.keyCode == 13 && $('#newElem').val().trim().length) {

				// if there isn't button 'add' 
				if ($('#add').css('display') != 'none') {

					addItem();
				} else {

					updateItem();
				}
			}
		});

		// COMPLETED ITEM

		$(document).on('change', 'input:checkbox', function() {
			var item = $(this).next('span');

			// if completed
			if (this.checked) {

				// crossed out item
				$(item).addClass('completed');
				$(this).attr( 'checked', 'checked' );

				if ($('#hide').css('display') != 'none') {
					localStorage.setItem('todo', $('#list').html()); // save session
				}

				// if there is button 'hide'
				if ($('#hide').css('display') == 'none') {
					hideItem();
					$('#show').remove(); // remove button 'show'
				}

			} else {
				$(item).removeClass('completed');
				$(this).removeAttr('checked');

				if ($('#hide').css('display') != 'none') {
					localStorage.setItem('todo', $('#list').html()); // save session
				}
			}
		});

		// EDIT ITEM

		$(document).on('click', '#edit', function() {

			// if there isn't button 'show'
			if ($('#show').css('display') != 'none') {
				$('#show').addClass('is');
			}

			var item = $(this).prev('span').text();

			$('#newElem').focus();

			// add attribute id (span id='...')
			$(this).prev('span').attr('id', 'editItem');

			// add item in main input
			$('#newElem').val(item);

			var updateBtn = '<button id="update">update</button>';
			
			$('#newElem').after(updateBtn);

			$('#add').hide();
			$('#hide').hide();
			$('#show').hide();
			$('#list').find('li').hide();

			localStorage.setItem('todo', $('#list').html()); // save session
		});

		// UPDATE ITEM

		$(document).on('click', '#update', function() {

			// if something is written
			if ($('#newElem').val().trim().length) {
				updateItem();
			} else {
				$('#newElem').focus();
			}
		});

		// DELETE ITEM

		$(document).on('click', '#delete', function() {
			$(this).parent('li').remove();
			localStorage.setItem('todo', $('#list').html()); // save session
		});

		// HIDE COMPLETED ITEMS

		$(document).on('click', '#hide', hideItem);

		// SHOW COMPLETED ITEMS

		$(document).on('click', '#show', function() {

			// show all crossed out items
			$("input:checkbox:checked").parent('li').show();
			
			$('#show').remove();
			$('#hide').show();
		});
});